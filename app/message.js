const express = require('express');
const fileDb = require('../fileDb');
const router = express.Router();

router.get('/', (req, res) => {
  const messages = fileDb.getItems();
  res.send(messages);
});

router.get('/:id', (req, res) => {
  const message = fileDb.getItems(req.params.id);
  if(!message) {
    return res.status(404).send({error: 'Product not found'})
  }
  res.send(message);
});

router.post('/', (req, res) => {
  if(!req.body.message) {
    return res.status(400).send({error: 'data not valid'})
  }
  const newMessage = fileDb.addItem({
    message: req.body.message
  });

  res.send(newMessage);
});

module.exports = router;
const fs = require('fs');

module.exports = {
  getItems() {
    const arr = [];
    const files = fs.readdirSync('./messages');
    files.forEach(file => {
      const fileData = fs.readFileSync(`./messages/${file}`, 'utf-8')
      arr.push(JSON.parse(fileData));
    });
    return arr;
  },
  addItem(item) {
    const date = new Date();
    item.id = date.toISOString();
    const fileName = `./messages/${item.id}.txt`;
    this.save(fileName, item);
    return item;
  },
  save(filename, item) {
    fs.writeFileSync(filename, JSON.stringify(item));
  }
};